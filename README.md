# Microstick II Plus #

## About ##
This is a root folder for all my projects exclusively using the Micro-stick II Plus development boards.


## Hardware Specs ##
- Microchip Micro-stick II Board: https://www.microchip.com/Developmenttools/ProductDetails/DM330013-2
- Micro-stick Plus Rev C: http://en.microstickplus.com/home/doc


## Software Specs ##
- Environment: MPLAB X IDE v5.20
- Compiler used: XC16 v1.33
- Target Device(s): 
		-dsPIC33FJ64MC802
		-PIC24HJ64GP502
		-PIC24HJ128GP502
		-PIC23MX250F128B

## Developer Contact ##
- Joe Wierzbicki (joe@wheresbicki.com)