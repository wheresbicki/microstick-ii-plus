/*
 * Use of the LED-s and the button
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: main.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Description:
 * During the button on the panel is pushed, the blue LED-s,
 * in opposite case the red LED-s are lighting.
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include "HardwareProfile.h"
#include "typedefs.h"
//NOTE: PIC32MX250F128B does not have an RB12 I/O pin. Use one of the other micros for this.
/* Setting up configuration bits */
#if defined(__PIC24HJ64GP502__) || defined(__dsPIC33FJ64MC802__) || defined(__PIC24HJ128GP502__) || defined(__dsPIC33FJ128MC802__) /*|| defined(__PIC32MX250F128B)*/
// FOSCSEL
#pragma config FNOSC = FRC              // Oscillator Mode (Internal Fast RC (FRC))
// FOSC
#pragma config FCKSM = CSECMD           // Clock Switching and Monitor (Clock switching is enabled, Fail-Safe Clock Monitor is disabled)
#pragma config POSCMD = NONE            // Primary Oscillator Source (Primary Oscillator Disabled)
#pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 pin has digital I/O function)
#pragma config IOL1WAY = OFF           // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
  //_FOSCSEL(FNOSC_FRC);                              // Use internal oscillator  (FOSC ~ 8 Mhz)
  //_FOSC(FCKSM_CSECMD & OSCIOFNC_ON & POSCMD_NONE);  // RA3 pin is output
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)
  //_FWDT(FWDTEN_OFF);                                // Watchdog timer is disabled
#pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)
  //_FICD(JTAGEN_OFF);                                // JTAG Port is disabled
#else
  #error "The config bits aren't configured! Please, configure it!"
#endif


/* Function prototypes */
void vInitPLLIntOsc( void );
void vInitApp( void );

/*---------------------------------------------------------------------
  Function name: main
  Description: Entry point of the program
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
int main( void )
{
    vInitApp(); // Hardware initialization

    /* Infinite loop */
    while(1)
    {
        // Read the state of the button
        if( mGetButtonState() )
        {
            // If the button is pressed:
            mWriteLEDs(0b1100); // Turn on the LEDs with blue color
        }
        else
        {
            // If the button is not pressed:
            mWriteLEDs(0b0011); // Turn on the LEDs with red color
        }
    }
}

/*---------------------------------------------------------------------
  Function name: vInitApp
  Description: Startup initializations
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitApp( void )
{
    vInitPLLIntOsc(); // Initialization of PLL
    mTurnOnPower();   // Turn on 3.3V power pin
    mInitLEDs();      // Initialization of LEDs
    mInitButton();    // Initialization of Button
}

/*---------------------------------------------------------------------
  Function name: vInitPLLIntOsc
  Description: Configuring internal oscillator with PLL (7.37 MHz -> 80 MHz )
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitPLLIntOsc( void )
{
    PLLFBD = 41;            // M = 43
    CLKDIVbits.PLLPOST=0;   // N1 = 2
    CLKDIVbits.PLLPRE=0;    // N2 = 2

    __builtin_write_OSCCONH(0x01);
    __builtin_write_OSCCONL(0x01);

    while (OSCCONbits.COSC != 0b001);
    while(OSCCONbits.LOCK != 1);
}