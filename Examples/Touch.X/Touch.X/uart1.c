/*
 * Using UART1 module
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: uart1.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include "HardwareProfile.h"
#include "typedefs.h"
#include "uart1.h"

/*---------------------------------------------------------------------
  Function name: vInitU1
  Description: Initialization of UART1 module
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitU1 ( void )
{
	U1MODE = 0; // Clear UART1 mode register
	U1STA = 0;	// Clear UART1 status register

	U1BRG = (FCY/(16*BAUD_RATE_UART1))-1; // Calculate value of baud rate register

#if ((MICROSTICKPLUS_REV == RevA) || (MICROSTICKPLUS_REV == RevB))
  _RP11R = 0b00011;   // U1TX output -> RP11 pin
  _U1RXR = 10;        // U1RX input  <- RP10 pin
#elif (MICROSTICKPLUS_REV == RevC)
  _RP10R = 0b00011;   // U1TX output -> RP10 pin
  _U1RXR = 11;        // U1RX input  <- RP11 pin
#else
#error Revision number of Microstick Plus board is not defined ( MICROSTICKPLUS_REV ). Please define it in HardwareProfile.h file.
#endif

	U1MODEbits.UARTEN = 1;   // Enable UART1 module
	U1STAbits.UTXEN = 1;     // Enable UART1 transmit 
}


/*---------------------------------------------------------------------
  Function name: vPutCharU1
  Description: Sends one character to UART1 (use of blocking wait)
  Input parameters: Character to send
  Output parameters: -
-----------------------------------------------------------------------*/
void vPutCharU1 ( CHAR cChar )
{
	while (U1STAbits.UTXBF); // Waits when the output buffer is full
	U1TXREG = cChar;         // Puts the character to the buffer
}

/*---------------------------------------------------------------------
  Function name: vPutStrU1
  Description: Sends string to UART1
  Input parameters: String to send
  Output parameters: -
-----------------------------------------------------------------------*/
void vPutStrU1 ( CHAR* pcStr )
{
	while( *pcStr != 0 )
	{
		vPutCharU1(*pcStr++);
	}
}

/*---------------------------------------------------------------------
  Function name: cGetCharU1
  Description: Receives character from UART1 (use of blocking wait)
  Input parameters: -
  Output parameters: Received character
-----------------------------------------------------------------------*/
CHAR cGetCharU1( void )
{
    while ( !U1STAbits.URXDA ); // Waits when the input buffer is empty
    return U1RXREG;             // Returns with the received character
}
