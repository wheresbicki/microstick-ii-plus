/*
 * Using UART1 module
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: uart1.h
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#ifndef _UART1_H
#define _UART1_H

#include "typedefs.h"

/* Speed of the UART1 module */
#define BAUD_RATE_UART1	9600L

/*---------------------------------------------------------------------------
  MACRO: True if the serial ports input buffer contains received data
---------------------------------------------------------------------------*/
#define mIsU1RXDataAvailable() (U1STAbits.URXDA)

/*---------------------------------------------------------------------
  Function name: vInitU1
  Description: Initialization of UART1 module
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitU1 ( void );

/*---------------------------------------------------------------------
  Function name: vPutCharU1
  Description: Sends one character to UART1 (use of blocking wait)
  Input parameters: Character to send
  Output parameters: -
-----------------------------------------------------------------------*/
void vPutCharU1 ( CHAR cChar );

/*---------------------------------------------------------------------
  Function name: vPutStrU1
  Description: Sends string to UART1
  Input parameters: String to send
  Output parameters: -
-----------------------------------------------------------------------*/
void vPutStrU1 ( CHAR* pcStr );

/*---------------------------------------------------------------------
  Function name: cGetCharU1
  Description: Receives character from UART1 (use of blocking wait)
  Input parameters: -
  Output parameters: Received character
-----------------------------------------------------------------------*/
CHAR cGetCharU1( void );

#endif
