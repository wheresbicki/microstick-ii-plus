/*
 * Reading value of capacitive button
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: touch.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include "HardwareProfile.h"
#include "typedefs.h"
#include "touch.h"

/*---------------------------------------------------------------------
  Function name: xReadCapTouch
  Description: Reading value of capacitive button
  Input parameters: -
  Output parameters: Current value of the button
-----------------------------------------------------------------------*/
BASE_TYPE xReadCapTouch( void )
{
    volatile UBASE_TYPE uxIndex;

    AD1CON1 = 0;
    AD1CON1bits.AD12B = 1;  // Mode: 12 bit
    AD1CON1bits.SSRC = 7;   // Auto convert
    AD1CON2 = 0;
    AD1CON3 = 0;
    AD1CON3bits.ADRC = 1;   // Use internal oscillator
    AD1CON3bits.SAMC = 31;
    AD1CON4 = 0;
    AD1CHS123 = 0;
    AD1CHS0 = 5;
    AD1PCFGL &= ~(1<<5);
    _AD1IE = 0;
    AD1CON1bits.ADON = 1;	//Turn on ADC

    _LATB3 = 1;                 // Set Vdd on pin 
    _TRISB3 = 0;                // Set pin to output
    for(uxIndex=0xFFFF; uxIndex!=0; uxIndex-- ) Nop();  // Waiting
    _TRISB3 = 1;                // Set pin to input

    AD1CON1bits.SAMP = 1;
    while(!AD1CON1bits.DONE);

    return ADC1BUF0;
}
