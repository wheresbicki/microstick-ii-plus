/*
 * Display measured value of capacitive button using UART1 serial port
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: main.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Description:
 * The sample program based on the Capacitive Voltage Divider (CVD) method.
 * The main idea of the method is that the PCB touch sensor capacity and the A/D converter
 * sampling/holding condensator capacity are in a comparable region. If we approach to the
 * touch sensor, the capacity of the touch sensor changes and this change is measurable.
 * The program performs the first measurement in initialization part, and after compares
 * the further measured values with the first measured value. The program write the measured
 * voltages to the UART1 serial port. If the voltage of the touch changed with 6,5% compared
 * to the initialization measure, then the program considers that the touch was pushed.
 * The state of the capcitive button is displayed on the LED line.
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include <stdio.h>
#include <math.h>
#include "HardwareProfile.h"
#include "typedefs.h"
#include "uart1.h"
#include "touch.h"

/* Setting up configuration bits */
#if defined(__PIC24HJ64GP502__) || defined(__dsPIC33FJ64MC802__) || defined(__PIC24HJ128GP502__) || defined(__dsPIC33FJ128MC802__)
//  _FOSCSEL(FNOSC_FRC);                              // Use internal oscillator  (FOSC ~ 8 Mhz)
//  _FOSC(FCKSM_CSECMD & OSCIOFNC_ON & POSCMD_NONE);  // RA3 pin is output
//  _FWDT(FWDTEN_OFF);                                // Watchdog timer is disabled
//  _FICD(JTAGEN_OFF);                                // JTAG Port is disabled
    #pragma config FNOSC = FRC              // Oscillator Mode (Internal Fast RC (FRC))
    #pragma config FCKSM = CSECMD           // Clock Switching and Monitor (Clock switching is enabled, Fail-Safe Clock Monitor is disabled)
    #pragma config POSCMD = NONE            // Primary Oscillator Source (Primary Oscillator Disabled)
    #pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 pin has digital I/O function)
    #pragma config IOL1WAY = OFF           // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
    #pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)
    #pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)
#else
  #error "The config bits aren't configured! Please, configure it!"
#endif

/* Function prototypes */
void vInitPLLIntOsc( void );
void vInitApp( void );

/*---------------------------------------------------------------------
  Function name: main
  Description: Entry point of the program
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
int main( void )
{
    FLOAT fValue;
    FLOAT fLast;
    BASE_TYPE xCapValue, xCapCompValue;

    vInitApp();                   // Hardware initialization

    // Dummy read
    xReadCapTouch();

    // Reference value of capacitive button is the average of the firs 4 read sequence
    xCapCompValue = xReadCapTouch();
    xCapCompValue += xReadCapTouch();
    xCapCompValue += xReadCapTouch();
    xCapCompValue += xReadCapTouch();
    xCapCompValue >>=2;

    // The comparator value equals to the reference value + 6.25%
    xCapCompValue += xCapCompValue >> 4;
    fValue = (FLOAT)xCapCompValue*(3300.0/4096.0);
    printf("\n\nVcmp = %.2f mV \r\n", (double)fValue);

    /* Infinite loop */
    while(1)
    {
        // Reads the value of capacitive button
        xCapValue = xReadCapTouch();
        fValue = (FLOAT)xCapValue*(3300.0/4096.0);
        if(fabs((fValue - fLast))> 50.0){  // add a low filter condition to eliminate noise in terminal
            printf("Vcap = %.2f mV   \r ", (double)fValue);
        }
        fLast = fValue;
        if (xCapValue > xCapCompValue)
        {
            // The button is touched
            mWriteLEDs(0b1100);
        }
        else
        {
            // The button is not touched
            mWriteLEDs(0b0011)
        }
    }
}

/*---------------------------------------------------------------------
  Function name: vInitApp
  Description: Startup initializations
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitApp( void )
{
    vInitPLLIntOsc(); // Initialization of PLL
    mTurnOnPower();   // Turn on 3.3V power pin
    mInitLEDs();      // Initialization of LEDs
    mInitButton();    // Initialization of Button
    vInitU1();        // Initialization of UART1 module
}

/*---------------------------------------------------------------------
  Function name: vInitPLLIntOsc
  Description: Configuring internal oscillator with PLL (7.37 MHz -> 80 MHz )
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitPLLIntOsc( void )
{
    PLLFBD = 41;            // M = 43
    CLKDIVbits.PLLPOST=0;   // N1 = 2
    CLKDIVbits.PLLPRE=0;    // N2 = 2

    __builtin_write_OSCCONH(0x01);
    __builtin_write_OSCCONL(0x01);

    while (OSCCONbits.COSC != 0b001);
    while(OSCCONbits.LOCK != 1);
}
