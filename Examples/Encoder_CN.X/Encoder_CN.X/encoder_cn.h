/*
 * Using Rotary encoder with Input Change Notification Interrupt
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: encoder_cn.h
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#ifndef _ENCODER_CN_H
#define _ENCODER_CN_H

#include "typedefs.h"

/* Global variables */
extern volatile BASE_TYPE xRotaryCounter; // Rotary Encoder counter

/*---------------------------------------------------------------------
  Function name: vInitCN
  Description: Initialization of Input Change Notification Interrupt on RB6, RB7 pin
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitCN( void );

#endif