/*
 * Using Rotary encoder with Change Notification Interrupt
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: main.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Description:
 * The rotary encoder which is located on the development board is
 * connected to the RB6 and RB7 pins of the microcontroller.
 * The encoder generates 90o shifted square signs due to turning.
 * The direction of the turning can be determined by the relative
 * position of the two signals. The program uses the Change Notification
 * Interrupt. When the values of RB6 or RB7 pins change, the program
 * reads the values of both pins and determines the direction of the
 * turning. The interrupt service routine decrements or increments the
 * value of a global variable based on the conditions. The main program
 * shows the value of the global variable on the LED line. The displayed
 * LED design can be changed with the help of the button on the board.
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include "HardwareProfile.h"
#include "typedefs.h"
#include "encoder_cn.h"

/* Setting up configuration bits */
#if defined(__PIC24HJ64GP502__) || defined(__dsPIC33FJ64MC802__) || defined(__PIC24HJ128GP502__) || defined(__dsPIC33FJ128MC802__)
    #pragma config FNOSC = FRC              // Oscillator Mode (Internal Fast RC (FRC))
    #pragma config FCKSM = CSECMD           // Clock Switching and Monitor (Clock switching is enabled, Fail-Safe Clock Monitor is disabled)
    #pragma config POSCMD = NONE            // Primary Oscillator Source (Primary Oscillator Disabled)
    #pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 pin has digital I/O function)
    #pragma config IOL1WAY = OFF           // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
    #pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)
    #pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)
#else
  #error "The config bits aren't configured! Please, configure it!"
#endif

/* Function prototypes */
void vInitPLLIntOsc( void );
void vInitApp( void );

/*---------------------------------------------------------------------
  Function name: main
  Description: Entry point of the program
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
int main( void )
{
    vInitApp(); // Hardware initialization

    /* Infinite loop */
    while(1)
    {
        // Read the state of the button
        if( mGetButtonState() )
        {
          // If the button is pressed:
            mWriteLEDs(xRotaryCounter>>2); // xRotaryCounter divided by 4
        }
        else
        {
          // If the button is not pressed:
            mWriteLEDs(1<<((xRotaryCounter>>2)&0b11));
        }
    }
}

/*---------------------------------------------------------------------
  Function name: vInitApp
  Description: Startup initializations
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitApp( void )
{
    vInitPLLIntOsc(); // Initialization of PLL
    mTurnOnPower();   // Turn on 3.3V power pin
    mInitLEDs();      // Initialization of LEDs
    mInitButton();    // Initialization of Button
    vInitCN();        // Initialization of Rotary Encoder with CN module
}

/*---------------------------------------------------------------------
  Function name: vInitPLLIntOsc
  Description: Configuring internal oscillator with PLL (7.37 MHz -> 80 MHz )
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitPLLIntOsc( void )
{
    PLLFBD = 41;            // M = 43
    CLKDIVbits.PLLPOST=0;   // N1 = 2
    CLKDIVbits.PLLPRE=0;    // N2 = 2

    __builtin_write_OSCCONH(0x01);
    __builtin_write_OSCCONL(0x01);

    while (OSCCONbits.COSC != 0b001);
    while(OSCCONbits.LOCK != 1);
}
