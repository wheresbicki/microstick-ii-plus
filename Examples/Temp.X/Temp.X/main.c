/*
 * Display measured temperature using UART1 serial port
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: main.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Description:
 * The sample program measures the TC1047 analog temperature 
 * sensors output voltage and sends the measured temperature
 * to the UART1 serial port in Celsius degree.
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include <stdio.h>
#include "HardwareProfile.h"
#include <libpic30.h>        // __delayXXX() functions macros defined here
#include "typedefs.h"
#include "uart1.h"
#include "ad1.h"

/* Setting up configuration bits */
#if defined(__PIC24HJ64GP502__) || defined(__dsPIC33FJ64MC802__) || defined(__PIC24HJ128GP502__) || defined(__dsPIC33FJ128MC802__)
// These macros are depreciated in later XC compilers
//  _FOSCSEL(FNOSC_FRC);                              // Use internal oscillator  (FOSC ~ 8 Mhz)
//  _FOSC(FCKSM_CSECMD & OSCIOFNC_ON & POSCMD_NONE);  // RA3 pin is output
//  _FWDT(FWDTEN_OFF);                                // Watchdog timer is disabled
//  _FICD(JTAGEN_OFF);                                // JTAG Port is disabled
    #pragma config FNOSC = FRC              // Oscillator Mode (Internal Fast RC (FRC))
    #pragma config FCKSM = CSECMD           // Clock Switching and Monitor (Clock switching is enabled, Fail-Safe Clock Monitor is disabled)
    #pragma config POSCMD = NONE            // Primary Oscillator Source (Primary Oscillator Disabled)
    #pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 pin has digital I/O function)
    #pragma config IOL1WAY = OFF           // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
    #pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)
    #pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)
#else
  #error "The config bits aren't configured! Please, configure it!"
#endif

/* Function prototypes */
void vInitPLLIntOsc( void );
void vInitApp( void );

/*---------------------------------------------------------------------
  Function name: main
  Description: Entry point of the program
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
int main( void )
{
    FLOAT fValue;                 // Local variable

    vInitApp();                   // Hardware initialization

    /* Infinite loop */
    while(1)
    {
        fValue = (FLOAT)xReadADC1(1)*(330.0/4096.0)-50.0;
        __delay_ms(250);
        printf("Temperature: %.2f C    \r", (double)fValue);
    }
}

/*---------------------------------------------------------------------
  Function name: vInitApp
  Description: Startup initializations
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitApp( void )
{
    vInitPLLIntOsc(); // Initialization of PLL
    mTurnOnPower();   // Turn on 3.3V power pin
    mInitLEDs();      // Initialization of LEDs
    mInitButton();    // Initialization of Button
    vInitU1();        // Initialization of UART1 module
}

/*---------------------------------------------------------------------
  Function name: vInitPLLIntOsc
  Description: Configuring internal oscillator with PLL (7.37 MHz -> 80 MHz )
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitPLLIntOsc( void )
{
    PLLFBD = 41;            // M = 43
    CLKDIVbits.PLLPOST=0;   // N1 = 2
    CLKDIVbits.PLLPRE=0;    // N2 = 2

    __builtin_write_OSCCONH(0x01);
    __builtin_write_OSCCONL(0x01);

    while (OSCCONbits.COSC != 0b001);
    while(OSCCONbits.LOCK != 1);
}
