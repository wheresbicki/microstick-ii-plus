/*
 * Using UART1 module with MCP2200
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: main.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Description:
 * The sample program communicates with the connected computer with the help of a MCP2200 
 * USB to serial converter located on the panel. The program sends to computer a sample
 * text after starting, and then receives characters from the serial port. The program
 * assigns the lowest 4 bits of the ASCII value of received character to the LED line and
 * sends back the character value to the serial port.
 * Settings of the serial port:
 * Transmission rate: 9600 baud, 8 bit transmission without parity bit, using 1 stop bit.
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include <stdio.h>
#include "HardwareProfile.h"
#include "typedefs.h"
#include "uart1.h"

/* Setting up configuration bits */
#if defined(__PIC24HJ64GP502__) || defined(__dsPIC33FJ64MC802__) || defined(__PIC24HJ128GP502__) || defined(__dsPIC33FJ128MC802__)
// These macros are depreciated in later XC compilers
//  _FOSCSEL(FNOSC_FRC);                              // Use internal oscillator  (FOSC ~ 8 Mhz)
//  _FOSC(FCKSM_CSECMD & OSCIOFNC_ON & POSCMD_NONE);  // RA3 pin is output
//  _FWDT(FWDTEN_OFF);                                // Watchdog timer is disabled
//  _FICD(JTAGEN_OFF);                                // JTAG Port is disabled
    #pragma config FNOSC = FRC              // Oscillator Mode (Internal Fast RC (FRC))
    #pragma config FCKSM = CSECMD           // Clock Switching and Monitor (Clock switching is enabled, Fail-Safe Clock Monitor is disabled)
    #pragma config POSCMD = NONE            // Primary Oscillator Source (Primary Oscillator Disabled)
    #pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 pin has digital I/O function)
    #pragma config IOL1WAY = OFF           // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
    #pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)
    #pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)
#else
  #error "The config bits aren't configured! Please, configure it!"
#endif

/* Function prototypes */
void vInitPLLIntOsc( void );
void vInitApp( void );

/*---------------------------------------------------------------------
  Function name: main
  Description: Entry point of the program
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
int main( void )
{
    CHAR cChar;                   // Local variable

    vInitApp();                   // Hardware initialization

    printf("Microstick Plus\n");  // Write welcome text to standard output

    /* Infinite loop */
    while(1)
    {
      cChar = cGetCharU1();       // Read one character form UART1
      mWriteLEDs(cChar);          // Dump to LEDs lower 4 bits of read character 
      vPutCharU1(cChar);          // Send back the read character
    }
}

/*---------------------------------------------------------------------
  Function name: vInitApp
  Description: Startup initializations
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitApp( void )
{
    vInitPLLIntOsc(); // Initialization of PLL
    mTurnOnPower();   // Turn on 3.3V power pin
    mInitLEDs();      // Initialization of LEDs
    mInitButton();    // Initialization of Button
    vInitU1();        // Initialization of UART1 module
}

/*---------------------------------------------------------------------
  Function name: vInitPLLIntOsc
  Description: Configuring internal oscillator with PLL (7.37 MHz -> 80 MHz )
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitPLLIntOsc( void )
{
    PLLFBD = 41;            // M = 43
    CLKDIVbits.PLLPOST=0;   // N1 = 2
    CLKDIVbits.PLLPRE=0;    // N2 = 2

    __builtin_write_OSCCONH(0x01);
    __builtin_write_OSCCONL(0x01);

    while (OSCCONbits.COSC != 0b001);
    while(OSCCONbits.LOCK != 1);
}
