/*
 * Drive buzzer with PWM using OC1 and Timer2 peripheral
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: buzzer.h
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#ifndef _BUZZER_H
#define _BUZZER_H

#include "typedefs.h"

/* Buzzer audio frequency */
#define PWM_PERIOD_FREQ	4000ul

/* Turn On and Turn Off Buzzer */
#define mBuzzerON()     {T2CONbits.TON = 1;}
#define mBuzzerOFF()    {T2CONbits.TON = 0;}

/* Function prototypes */
void vInitPWM( void );	// Initialization of Timer2 and OC1 module to generate PWM output

#endif
