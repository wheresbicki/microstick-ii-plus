/*
 *  Drive buzzer with PWM using OC1 and Timer2 peripheral
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: buzzer.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include "HardwareProfile.h"
#include "typedefs.h"
#include "buzzer.h"

/*---------------------------------------------------------------------
  Function name: vInitPWM
  Description: Initialization of Timer2 and OC1 module to generate PWM output
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitPWM( void )
{
	/* Configuring Timer2 */
	T2CON = 0; // Source: Fcy, Prescaler: 1:1, 16 bit mode.
	#if ((FCY/PWM_PERIOD_FREQ)-1) > 0xFFFFul
		#error The value of PR2 register is too high
	#endif
	PR2 = ((FCY/PWM_PERIOD_FREQ)-1);
	TMR2 = 0;
	_T2IF = 0;	// Clear Timer2 Interrupt Flag
	_T2IE = 0;	// Disable Timer2 Interrupt

	/* Configuring OC1 */
	OC1CON = 0; // Use Timer2 as input source
	OC1CONbits.OCM = 0b110; // PWM mode, without fault pin
	_OC1IF = 0;	// Clear OC1 Interrupt Flag
	_OC1IE = 0;	// Disable OC1 Interrupt

        OC1R = OC1RS = (FCY/PWM_PERIOD_FREQ)/2; // PWM Duty Cycle: 50%

	_RP5R = 0b10010;    // OC1 output is assigned to RP5 Output pin
}
