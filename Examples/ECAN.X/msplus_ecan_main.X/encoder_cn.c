/*
 * Using Rotary encoder with Input Change Notification Interrupt
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: encoder_cn.c
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#include <xc.h>
#include "HardwareProfile.h"
#include "typedefs.h"
#include "encoder_cn.h"

/* Global variables */
volatile BASE_TYPE xRotaryCounter; // Rotary Encoder counter

/*---------------------------------------------------------------------
  Function name: vInitCN
  Description: Initialization of Input Change Notification Interrupt on RB6, RB7 pin
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitCN( void )
{
	_CN23IE = 1;   // Enable Input Change Notification Interrupt on RB7 pin (Rotary)
	_CN24IE = 1;   // Enable Input Change Notification Interrupt on RB6 pin (Rotary)

	_CNIF = 0;	// Clear Input Change Notification Flag
	_CNIP = 1;	// Set priority of Input Change Notification Interrupt
	_CNIE = 1;	// Enable Input Change Notification Interrupt
}

/*---------------------------------------------------------------------
  Function name: _CNInterrupt
  Description: Input Change Notification Interrupt Service Routine
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void __attribute__((interrupt, no_auto_psv)) _CNInterrupt( void )
{
    static BASE_TYPE xPrevisionRotaryState;
    BASE_TYPE xCurrentRotaryState;

    IFS1bits.CNIF = 0;		// Clear Input Change Notification Flag

    // Calculate new value of Rotary Counter
    xCurrentRotaryState = mReadRotaryState();
    if (  xPrevisionRotaryState != xCurrentRotaryState  )
    {
        switch( xCurrentRotaryState )
        {
            case 0b00:
                if ( xPrevisionRotaryState == 0b10) xRotaryCounter++;
                if ( xPrevisionRotaryState == 0b01) xRotaryCounter--;
                break;
            case 0b01:
                if ( xPrevisionRotaryState == 0b00) xRotaryCounter++;
                if ( xPrevisionRotaryState == 0b11) xRotaryCounter--;
                break;
            case 0b11:
                if ( xPrevisionRotaryState == 0b01) xRotaryCounter++;
                if ( xPrevisionRotaryState == 0b10) xRotaryCounter--;
                break;
            case 0b10:
                if ( xPrevisionRotaryState == 0b11) xRotaryCounter++;
                if ( xPrevisionRotaryState == 0b00) xRotaryCounter--;
                break;
        }
        xPrevisionRotaryState = xCurrentRotaryState;
    }
}
