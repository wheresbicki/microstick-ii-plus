/*
 * Using ECAN module with DMA, sending messeges about Rotary encoder
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ128GP502
 *
 * File name: ecan_app.c
 * Author:    Gergely MARTON
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 20-09-2013
 */
#include <xc.h>
//#include "p24Hxxxx.h"
#include "HardwareProfile.h"
#include "ECAN1Config.h"
#include "common.h"
#include "encoder_cn.h"
#include "ecan_app.h"
#include "touch.h"

volatile BASE_TYPE xRotaryCounterTemp;  // Rotary Encoder counter previous value
#if ( LED_MODE == DIR_IND )
BYTE bLeds = 0;                         // Variable for LED values
#elif ( LED_MODE == SHIFT )
BYTE bLeds = 1;
#endif
mID rx_ecan1message;                    // CAN Messages in RAM
ECAN1MSGBUF ecan1msgBuf __attribute__((space(dma),aligned(ECAN1_MSG_BUF_LENGTH*16)));
                                        // Define CAN messege buffers
FLOAT fValue;
BASE_TYPE xCapValue, xCapCompValue;

/*---------------------------------------------------------------------
  Function name: vInitTouch
  Description: Initialization of touch button
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitTouch ( void )
{
    // Dummy read
    xReadCapTouch();

    // Reference value of capacitive button is the average of the firs 4 read sequence
    xCapCompValue = xReadCapTouch();
    xCapCompValue += xReadCapTouch();
    xCapCompValue += xReadCapTouch();
    xCapCompValue += xReadCapTouch();
    xCapCompValue >>=2;

    // The comparator value equals to the reference value + 6.25%
    xCapCompValue += xCapCompValue >> 4;
    fValue = (FLOAT)xCapCompValue*(3300.0/4096.0);
}

/*---------------------------------------------------------------------
  Function name: vReadTouch
  Description: Get touch state and call ECAN messege transmit
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vReadTouch ( void )
{
    static BYTE bState, bStateTemp;

    // Reads the value of capacitive button
    xCapValue = xReadCapTouch();
    fValue = (FLOAT)xCapValue*(3300.0/4096.0);

    if (xCapValue > xCapCompValue) bState = 1;  // The button is touched
    else bState = 0;                            // The button is not touched
    // If the button is pressed
    if( bState && !bStateTemp ) vEcanSendTouch ( PRESSED );
    // If the button is released:
    else if ( !bState && bStateTemp ) vEcanSendTouch ( RELEASED );

    bStateTemp = bState;
}

/*---------------------------------------------------------------------
  Function name: vReadButton
  Description: Get button state and call ECAN messege transmit
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vReadButton ( void )
{
    static BYTE bState, bStateTemp;

    bState = mGetButtonState();                 // Read the state of the button
    // If the button is pressed
    if( bState && !bStateTemp ) vEcanSendButton ( PRESSED );
    // If the button is released:
    else if ( !bState && bStateTemp ) vEcanSendButton ( RELEASED );

    bStateTemp = bState;
}

/*---------------------------------------------------------------------
  Function name: vReadEncoder
  Description: Get Rotary Encoder movement direction/steps and call ECAN
               messege transmit
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vReadEncoder ( void )
{
    if ( xRotaryCounterTemp != xRotaryCounter )             
    {
        if ( ( xRotaryCounter > ( xRotaryCounterTemp + 2 ) ) ||
             ( ( xRotaryCounterTemp == 0xFFFF ) && ( xRotaryCounterTemp == 0 ) ) )
        {
            vEcanSendEncoder( RIGHT );
            xRotaryCounterTemp = xRotaryCounter;
        }
        else if ( ( xRotaryCounter < ( xRotaryCounterTemp - 2 ) ) ||
                ( ( xRotaryCounterTemp == 0 ) && ( xRotaryCounterTemp == 0xFFFF ) ) )
        {
            vEcanSendEncoder( LEFT );
            xRotaryCounterTemp = xRotaryCounter;
        }
        
    }
}

/*---------------------------------------------------------------------
  Function name: vEcanSendTouch
  Description: Send ECAN messege containing touch button state
  Input parameters: State ( 1 - Pressed, 0 - Released )
  Output parameters: -
-----------------------------------------------------------------------*/
void vEcanSendTouch ( BYTE bState )
{
    while ( C1TR01CONbits.TXREQ0 );				// Waits for previous message to be sent successfully
    ecan1WriteTxMsgBufId(0,CAN_ID_TOUCH,0,0);                   // Buffer number: 0
                                                                // ID: 2
                                                                // Standard Identifier
                                                                // Standard Messege ( not Remote Transmit )

    ecan1WriteTxMsgBufData(0,1,bState,0,0,0);                   // Buffer Number: 0
                                                                // Byte Counter: 1
                                                                // Date Word 0 = State ( 1 - Pressed, 0 - Released )
    C1TR01CONbits.TXREQ0=1;                                     // Request transmit
}

/*---------------------------------------------------------------------
  Function name: vEcanSendButton
  Description: Send ECAN messege containing button state
  Input parameters: State ( 1 - Pressed, 0 - Released )
  Output parameters: -
-----------------------------------------------------------------------*/
void vEcanSendButton ( BYTE bState )
{
    while ( C1TR01CONbits.TXREQ0 );				// Waits for previous message to be sent successfully
    ecan1WriteTxMsgBufId(0,CAN_ID_BUTTON,0,0);                  // Buffer number: 0
                                                                // ID: 2
                                                                // Standard Identifier
                                                                // Standard Messege ( not Remote Transmit )

    ecan1WriteTxMsgBufData(0,1,bState,0,0,0);                   // Buffer Number: 0
                                                                // Byte Counter: 1
                                                                // Date Word 0 = State ( 1 - Pressed, 0 - Released )
    C1TR01CONbits.TXREQ0=1;                                     // Request transmit
}

/*---------------------------------------------------------------------
  Function name: vEcanSendEncoder
  Description: Send ECAN messege containing rotary encoder rotation direction
  Input parameters: Direction ( RIGHT, LEFT )
  Output parameters: -
-----------------------------------------------------------------------*/
void vEcanSendEncoder ( BYTE bDir )
{
    while ( C1TR01CONbits.TXREQ0 );				// Waits for previous message to be sent successfully
    ecan1WriteTxMsgBufId(0,CAN_ID_ENCODER,0,0);                 // Buffer number: 0
                                                                // ID: 1
                                                                // Standard Identifier
                                                                // Standard Messege ( not Remote Transmit )

    ecan1WriteTxMsgBufData(0,1,bDir,0,0,0);                     // Buffer Number: 0
                                                                // Byte Counter: 1
                                                                // Date Word 0 = Direction
    C1TR01CONbits.TXREQ0=1;                                     // Request transmit
}

/******************************************************************************
*
*   Function:       rxECAN1
*   Description:    Moves the message from the DMA memory to RAM
*
*   Arguments:      *message: a pointer to the message structure in RAM
*						that will store the message.
*   Author:         Jatinder Gharoo
*
******************************************************************************/
void rxECAN1(mID *message)
{
	unsigned int ide=0;
	unsigned int srr=0;
	unsigned long id=0;/*,d*/

	/*
	Standard Message Format:
	Word0 : 0bUUUx xxxx xxxx xxxx
			     |____________|||
 					SID10:0   SRR IDE(bit 0)
	Word1 : 0bUUUU xxxx xxxx xxxx
			   	   |____________|
						EID17:6
	Word2 : 0bxxxx xxx0 UUU0 xxxx
			  |_____||	     |__|
			  EID5:0 RTR   	  DLC
	word3-word6: data bytes
	word7: filter hit code bits

	Substitute Remote Request Bit
	SRR->	"0"	 Normal Message
			"1"  Message will request remote transmission

	Extended  Identifier Bit
	IDE-> 	"0"  Message will transmit standard identifier
	   		"1"  Message will transmit extended identifier

	Remote Transmission Request Bit
	RTR-> 	"0"  Message transmitted is a normal message
			"1"  Message transmitted is a remote message
	*/
	/* read word 0 to see the message type */
	ide=ecan1msgBuf[message->buffer][0] & 0x0001;
	srr=ecan1msgBuf[message->buffer][0] & 0x0002;

	/* check to see what type of message it is */
	/* message is standard identifier */
	if(ide==0)
	{
		message->id=(ecan1msgBuf[message->buffer][0] & 0x1FFC) >> 2;
		message->frame_type=CAN_FRAME_STD;
	}
	/* mesage is extended identifier */
	else
	{
		id=ecan1msgBuf[message->buffer][0] & 0x1FFC;
		message->id=id << 16;
		id=ecan1msgBuf[message->buffer][1] & 0x0FFF;
		message->id=message->id+(id << 6);
		id=(ecan1msgBuf[message->buffer][2] & 0xFC00) >> 10;
		message->id=message->id+id;
		message->frame_type=CAN_FRAME_EXT;
	}
	/* check to see what type of message it is */
	/* RTR message */
	if(srr==1)
	{
		message->message_type=CAN_MSG_RTR;
	}
	/* normal message */
	else
	{
		message->message_type=CAN_MSG_DATA;
		message->data[0]=(unsigned char)ecan1msgBuf[message->buffer][3];
		message->data[1]=(unsigned char)((ecan1msgBuf[message->buffer][3] & 0xFF00) >> 8);
		message->data[2]=(unsigned char)ecan1msgBuf[message->buffer][4];
		message->data[3]=(unsigned char)((ecan1msgBuf[message->buffer][4] & 0xFF00) >> 8);
		message->data[4]=(unsigned char)ecan1msgBuf[message->buffer][5];
		message->data[5]=(unsigned char)((ecan1msgBuf[message->buffer][5] & 0xFF00) >> 8);
		message->data[6]=(unsigned char)ecan1msgBuf[message->buffer][6];
		message->data[7]=(unsigned char)((ecan1msgBuf[message->buffer][6] & 0xFF00) >> 8);
		message->data_length=(unsigned char)(ecan1msgBuf[message->buffer][2] & 0x000F);
	}
}

void vSetUserFilters ( void )
{
    #ifdef ACCEPT_ROTARY
    ecan1WriteRxAcptFilter(0,CAN_ID_ENCODER,0,1,0);
                                                // Filter 0 - Rotary encoder messeges
                                                // Filter: 00000000001
                                                // Match messages with standard identifier addresses
                                                // RX buffer 0
                                                // Acceptance Mask 0 register contains mask
    #endif
    #ifdef ACCEPT_BUTTON
    ecan1WriteRxAcptFilter(1,CAN_ID_BUTTON,0,1,0);
                                                // Filter 1 - Button messeges
                                                // Filter: 00000000010
                                                // Match messages with standard identifier addresses
                                                // RX buffer 0
                                                // Acceptance Mask 0 register contains mask
    #endif
    #ifdef ACCEPT_TOUCH
    ecan1WriteRxAcptFilter(2,CAN_ID_TOUCH,0,1,0);
                                                // Filter 2 - Touch messeges
                                                // Filter: 00000000011
                                                // Match messages with standard identifier addresses
                                                // RX buffer 0
                                                // Acceptance Mask 0 register contains mask
    #endif
}
/*---------------------------------------------------------------------
  Function name: vHandleLeds
  Description: Handle input CAN massege containing rotary encoder values and
               put the corresponding value to LED PORT
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vHandleLeds ( void )
{
    if( rx_ecan1message.id == CAN_ID_ENCODER )                  // Check CAN messege ID for encoder messege
    {
        if ( rx_ecan1message.data[0] == RIGHT )                 // Get the direction from CAN messege byte 0
        {
            #if ( LED_MODE == DIR_IND )
            bLeds = 0b0011;                                     // Indicate direction right
            #endif
            #if ( LED_MODE == SHIFT )
            bLeds = bLeds >> 1;                                 // Shift LEDs right
            if ( !bLeds ) bLeds = 0b1000;
            #endif
        }
        if ( rx_ecan1message.data[0] == LEFT )
        {
            #if ( LED_MODE == DIR_IND )
            bLeds = 0b1100;                                     // Indicate direction right
            #endif
            #if ( LED_MODE == SHIFT )
            bLeds = bLeds << 1;                                 // Shift LEDs left
            if ( bLeds >= 0b10000 ) bLeds = 1;
            #endif
        }
    }
    else if( rx_ecan1message.id == CAN_ID_BUTTON )              // Check CAN messege ID for button messege
    {
        if ( rx_ecan1message.data[0] == PRESSED )               // Get the direction from CAN messege byte 0
        {
            bLeds = 0b1111;
        }
        if ( rx_ecan1message.data[0] == RELEASED )
        {
           bLeds = 0b0000;
        }
    }
    else if( rx_ecan1message.id == CAN_ID_TOUCH )               // Check CAN messege ID for touch messege
    {
        if ( rx_ecan1message.data[0] == PRESSED )               // Get the direction from CAN messege byte 0
        {
            bLeds = 0b1111;
        }
        if ( rx_ecan1message.data[0] == RELEASED )
        {
           bLeds = 0b0000;
        }
    }

    mWriteLEDs( bLeds );                                        // Put LED values to PORT
}