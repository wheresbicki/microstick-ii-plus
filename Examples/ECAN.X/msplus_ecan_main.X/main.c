/*
 * Using ECAN module with DMA, sending messeges about Rotary encoder
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ128GP502
 *
 * File name: main.c
 * Author:    Gergely MARTON
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 20-09-2013
 */
#include <xc.h>
//#include "p24Hxxxx.h"
#include "HardwareProfile.h"
#include "ECAN1Config.h"
#include "common.h"
#include "encoder_cn.h"
#include "ecan_app.h"
#include "touch.h"


/* Setting up configuration bits */

//  _FOSCSEL(FNOSC_FRC);                              // Use internal oscillator  (FOSC ~ 8 Mhz)
//  _FOSC(FCKSM_CSECMD & OSCIOFNC_ON & POSCMD_NONE);  // RA3 pin is output
//  _FWDT(FWDTEN_OFF);                                // Watchdog timer is disabled
//  _FICD(JTAGEN_OFF);                                // JTAG Port is disabled
    #pragma config FNOSC = FRC              // Oscillator Mode (Internal Fast RC (FRC))
    #pragma config FCKSM = CSECMD           // Clock Switching and Monitor (Clock switching is enabled, Fail-Safe Clock Monitor is disabled)
    #pragma config POSCMD = NONE            // Primary Oscillator Source (Primary Oscillator Disabled)
    #pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 pin has digital I/O function)
    #pragma config IOL1WAY = OFF           // Peripheral Pin Select Configuration (Allow Multiple Re-configurations)
    #pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog timer enabled/disabled by user software)
    #pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is Disabled)

/* Function prototypes */
void vInitPLLIntOsc( void );
void vInitApp( void );
void clearIntrflags(void);

int main(void) {

    vInitPLLIntOsc();               // PLL Initialization
    vInitApp();                     // Application Initialization
    clearIntrflags();               // Clear Interrupt Flags

/* Enable ECAN1 Interrupt */

    IEC2bits.C1IE = 1;
    C1INTEbits.TBIE = 1;
    C1INTEbits.RBIE = 1;

/* Loop infinitely */

    while (1)
    {
        vReadEncoder();             // Get rotary encoder state and send CAN messege
        vReadButton();              // Get button state and send CAN messege
        vReadTouch();               // Get touch button state and send CAN messege
    }
}

void vInitApp( void )
{
    vInitPLLIntOsc();               // Initialization of PLL
    mTurnOnPower();                 // Turn on 3.3V power pin
    mInitLEDs();                    // Initialization of LEDs
    mInitButton();                  // Initialization of Button
    vInitCN();                      // Initialization of Rotary Encoder
    vInitTouch();                   // Initialization of Touch button
    mWriteLEDs( bLeds );
    ecan1Init();                    // ECAN1 Initialisation
    dma0init();                     // Configure DMA Channel 0 for ECAN1 Transmit
    dma2init();                     // Configure DMA Channel 2 for ECAN1 Receive
    __builtin_write_OSCCONL(OSCCON & ~(1<<6));
    RPINR26bits.C1RXR = 8;          // CAN RX RP8
    RPOR4bits.RP9R = 0b10000;       // CAN RX RP9
    __builtin_write_OSCCONL(OSCCON | (1<<6));

    xRotaryCounter = xRotaryCounterTemp;
}

/*---------------------------------------------------------------------
  Function name: vInitPLLIntOsc
  Description: Configuring internal oscillator with PLL (7.37 MHz -> 80 MHz )
  Input parameters: -
  Output parameters: -
-----------------------------------------------------------------------*/
void vInitPLLIntOsc( void )
{
/*  Configure Oscillator to operate the device at 40Mhz
 	Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
 	Fosc= 8M*40/(2*2)=80Mhz for 8M input clock */

	PLLFBD=367;			/* M=369 */
	CLKDIVbits.PLLPOST=1;		/* N1=4 */
	CLKDIVbits.PLLPRE=15;		/* N2=17 */
	OSCTUN=0;			/* Tune FRC oscillator, if FRC is used */

/* Disable Watch Dog Timer */

	RCONbits.SWDTEN=0;

/* Wait for PLL to lock */

	while(OSCCONbits.LOCK!=1) {};
}

void clearIntrflags(void){
/* Clear Interrupt Flags */

	IFS0=0;
	IFS1=0;
	IFS2=0;
	IFS3=0;
	IFS4=0;
}

void __attribute__((interrupt, no_auto_psv))_C1Interrupt(void)
{
	IFS2bits.C1IF = 0;        // clear interrupt flag
	if(C1INTFbits.TBIF)
    {
    	C1INTFbits.TBIF = 0;
    }

    if(C1INTFbits.RBIF)
    {
        // read the message
	if(C1RXFUL1bits.RXFUL1==1)
	{
	   rx_ecan1message.buffer=1;
	   C1RXFUL1bits.RXFUL1=0;
	}
	rxECAN1(&rx_ecan1message);
	C1INTFbits.RBIF = 0;
        vHandleLeds();
    }
}

//------------------------------------------------------------------------------
//    DMA interrupt handlers
//------------------------------------------------------------------------------

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void)
{
   IFS0bits.DMA0IF = 0;          // Clear the DMA0 Interrupt Flag;
}

void __attribute__((interrupt, no_auto_psv)) _DMA2Interrupt(void)
{
   IFS1bits.DMA2IF = 0;          // Clear the DMA2 Interrupt Flag;
}

