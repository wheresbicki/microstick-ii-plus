#ifndef APP_H
#define	APP_H

#define ACCEPT_ROTARY
#define ACCEPT_BUTTON
#define ACCEPT_TOUCH

#define SHIFT       0                           // LED_MODE shift LEDs
#define DIR_IND     1                           // LED_MODE indicate direction

#define LED_MODE    DIR_IND                     // Configure LED mode for rotary messeges

#define RIGHT       0                           // Right direction for rotary encoder
#define LEFT        1                           // Left direction for rotary encoder

#define PRESSED     1
#define RELEASED    0
#define CAN_ID_ENCODER  1                       // CAN messege ID for rotary encoder messeges
#define CAN_ID_BUTTON   2                       // CAN messege ID for button messeges
#define CAN_ID_TOUCH    3                       // CAN messege ID for touch messeges

extern volatile BASE_TYPE xRotaryCounterTemp;   // Rotary Encoder counter previous value
extern BYTE bLeds;                              // Variable for LED values
extern mID rx_ecan1message;                     // CAN Messages in RAM

void vInitTouch ( void );                       // Initialization of touch button
void vReadTouch ( void );                       // Get touch state and call ECAN messege transmit
void vReadButton ( void );                      // Get button state and call ECAN messege transmit
void vReadEncoder ( void );                     // Get Rotary Encoder movement direction/steps
void vEcanSendTouch ( BYTE bState );            // Send ECAN messege containing touch button state
void vEcanSendButton ( BYTE bState );           // Send ECAN messege containing button state
void vEcanSendEncoder ( BYTE bDir );            // Send CAN Messege containing rotary encoder direction
void vHandleLeds ( void );                      // Shift LEDs based on incoming rotary CAN messeges
void rxECAN1(mID *message);                     // Moves the message from the DMA memory to RAM
void vSetUserFilters ( void );                      // Set user defined filters

#endif	/* APP_H */

