/*
 * Reading value of capacitive button
 *
 * Tested on:
 * Microstick + Microstick Plus Development Board
 * PIC24HJ64GP502
 *
 * File name: touch.h
 * Author:    Jozsef KOPJAK
 * Info:      info@microstickplus.com
 * Web:       http://www.microstickplus.com
 *
 * Last modification: 10-04-2013
 */

#ifndef _TOUCH_H
#define _TOUCH_H

#include "typedefs.h"

/*---------------------------------------------------------------------
  Function name: xReadCapTouch
  Description: Reading value of capacitive button
  Input parameters: -
  Output parameters: Current value of the button
-----------------------------------------------------------------------*/
BASE_TYPE xReadCapTouch( void );

#endif
