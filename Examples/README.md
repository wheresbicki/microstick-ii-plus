# Microstick II Plus Example Projects #
## About ##
Following projects are adopted from http://en.microstickplus.com/ examples and  were updated to remove depreciated XC syntax and library dependencies.


## Software Specs ##
- Environment: MPLAB X IDE v5.20
- Compiler used: XC16 v1.33
- Target Device(s): 
		-dsPIC33FJ64MC802
		-PIC24HJ64GP502
		-PIC24HJ128GP502
		-PIC23MX250F128B

## Developer Contact ##
- Joe Wierzbicki (joe@wheresbicki.com)